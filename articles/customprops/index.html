<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Articles - CSS Custom Properties</title>
        <link rel="stylesheet" href="../../resources/sh_bright.min.css">
        <script src="../../resources/sh_main.min.js"></script>
        <script src="../../resources/sh_javascript_dom.min.js"></script>
        <script src="../../resources/sh_css.min.js"></script>
        <script src="../../resources/sh_html.min.js"></script>
        <script src="../../resources/stringify.js"></script>
        <link rel="stylesheet" href="../../resources/styles.css">
    </head>
    <body>
        <nav>
            <a href="../../">Table of Contents</a>
        </nav>
        <article>
            <h1 id="top">CSS Custom Properties AKA "variables"</h1>
            <time pubdate>2016-05-08</time>
            <p class="notice">
                This is about an upcoming feature that is currently usable in the latest Firefox, Chrome, and Opera. Support is notably
                missing in IE and Edge. Check <a href="http://caniuse.com/#search=variables">Caniuse</a> for details.
            </p>
            <p>Many developers today use CSS pre-processors and if asked, would probably say they can't live without them. They can
            serve to reduce some of the work and clutter involved in making a large complex website. They can also do the opposite,
            however. General features of pre-processors include variables, the ability to include styles in one selector originating
            from another, functions, and the ability to nest selectors. The most used of these features seems to be variables.
            Perhaps as a result of this popularity, a <a href="https://www.w3.org/TR/css-variables-1/">spec</a> was proposed to provide
            this feature within CSS itself.</p>
            <p>The result is officially called Custom Properties and offers features similar to pre-processor variables but with an
            important and powerful distinction. This article will explore CSS custom properties, how they work, and their differences
            from <a href="http://lesscss.org/">LESS</a>, a popular CSS pre-processor.</p>
            <h2>A quick comparison</h2>
            <p>Ok, first, let's start with the familiar. The following is a simple LESS variable used in a single selector, next to the
            <span class="dfn" title="Simplified: Translating from one language to another of the a similar nature">transpiled</span>
            output of the same:</p>
            <div class="example">
<pre class="sh_css">
@cerulean: #1dacd6;

h1 {
    background: @cerulean;
}
</pre>
<pre class="sh_css">
h1 {
    background: #1dacd6;
}
</pre>
            </div>
            <p>In CSS, the following box on the left is the formal syntax. The box on the right is the effect of this syntax, as CSS is
            interpreted directly.</p>
            <div class="example">
<pre class="sh_css">
:root {
    --cerulean: #1dacd6;
}

h1 {
    background: var(--cerulean);
}
</pre>
<pre class="sh_css">
h1 {
    background: #1dacd6;
}
</pre>
            </div>
            <p>The first thing to notice is the output is exactly the same. In the basic use-case of global variables, CSS custom
            properties behave exactly the same as LESS's variables. The second, and perhaps most obvious thing, is that CSS doesn't
            allow properties declared outside of any sort of selector. However the <code>:root</code> selector is sufficient to define
            properties accessible everywhere for reasons I'll cover in the next section. Finally, all properties must start with two
            dashes in order to be valid and can only be retrieved by way of the <code>var</code> function. The dashes are presumably
            to ensure they will never clash with any future spec-defined non-custom CSS properties.</p>
            <h2>Scoping</h2>
            <p>This is where I tell you I lied. I'm sorry. In the previous section I indicated that custom properties behave exactly
            the same as pre-processor variables in the basic case. This is somewhat untrue. To show you, let's take for example a few
            different placements of LESS's variables</p>
            <div class="example">
<pre class="sh_css">
h1 {
    @cerulean: #1dacd6;
    
    span {
        background: @cerulean;
    }
}
</pre>
<pre class="sh_css">
h1 {
    @cerulean: #1dacd6;
}

h1 span {
    background: @cerulean;
}
</pre>
            </div>
            <p>and the resulting CSS output:</p>
            <div class="example">
<pre class="sh_css">
h1 span {
    background: #1dacd6;
}
</pre>
<pre class="sh_css">
NameError: variable @cerulean is undefined
</pre>
            </div>
            <p>As you'll see in the first case, LESS has no problems sharing the variable with nested selectors. However if we split up
            up the selectors, LESS complains about its inability to find the variable we requested.</p>
            <p>CSS on the other hand relies on the cascading (inheritance-based) nature of CSS to determine visibility of its custom
            properties. Since we cannot have nested selectors, we must settle for comparing only to the second example, shown on the
            left, and the effective behavior of this CSS, as shown on the right:</p>
            <div class="example">
<pre class="sh_css">
h1 {
    --cerulean: #1dacd6;
}

h1 span {
    background: var(--cerulean);
}
</pre>
<pre class="sh_css">
h1 {
}

h1 span {
    background: #1dacd6;
}
</pre>
            </div>
            <p>As the value is inherited, we can actually take this further and remove the <code>h1</code> from the second selector and
            the result will behave exactly the same in the browser. LESS, and other pre-processors, would be unable to perform this
            level of variable resolution as their work finishes at transpilation time. This is what makes CSS custom properties quite
            powerful.</p>
            <h2>Pitfalls and Gotchas</h2>
            <p>What makes this syntax powerful can also be a detriment for the unaware. Someone used to pre-processors may be surprised
            at the result if they define a custom property in the "global" (<code>:root</code>) scope, and a custom property with the
            same name in another selector when a third selector using the property happens to match an element that would inherit from
            the second declaration. This may be a little confusing, so let's take a look at what this would look like in LESS, along
            with some markup exhibiting the issue in question and the resulting output:</p>
            <div class="example">
<pre class="sh_css">
@cerulean: #1dacd6;

h3 {
    @cerulean: blue;
}

span {
    color: @cerulean;
}
</pre>
<pre class="sh_html">
<h3>Some <span>text</span> in a title</h3>
<p>More <span>text</span> in a paragraph.</p>
</pre>
<div>
    <h3>Some <span style="color: #1dacd6">text</span> in a title</h3>
    <p>More <span style="color: #1dacd6">text</span> in a paragraph.</p>
</div>
            </div>
            <p>Using the following direct translation to CSS, however, we get a much different result as indicated by the output:
            <div class="example">
<pre class="sh_css">
:root {
    --cerulean: #1dacd6;
}

h3 {
    --cerulean: blue;
}

span {
    color: var(--cerulean);
}
</pre>
<pre class="sh_html">
<h3>Some <span>text</span> in a title</h3>
<p>More <span>text</span> in a paragraph.</p>
</pre>
<div>
    <!-- Do forgive. In order to show how this would work in case someone visited from IE, I've faked it. I did test it first though. -->
    <h3>Some <span style="color: blue">text</span> in a title</h3>
    <p>More <span style="color: #1dacd6">text</span> in a paragraph.</p>
</div>
            </div>
            <p>Here, the second declaration of "<code>cerulean</code>" redefines the value at the h3 point in the hierarchy. Any element
            in position to inherit property values from an h3 does so, and the result is as seen above. One way to avoid this would be
            to only define custom properties in <code>:root</code>, but this would make it much harder to manage many custom
            properties. This can also be avoided by judicious naming of properties to reduce the chance of unintentional redeclaration.</p>
            <p>On the upside, we no longer need the "global" declaration if we only wish to target elements inheriting from an h3, as
            can be seen below:</p>
            <div class="example">
<pre class="sh_css">
h3 {
    --cerulean: #1dacd6;
}

span {
    color: var(--cerulean);
}
</pre>
<pre class="sh_html">
<h3>Some <span>text</span> in a title</h3>
<p>More <span>text</span> in a paragraph.</p>
</pre>
<div>
    <h3>Some <span style="color: #1dacd6">text</span> in a title</h3>
    <p>More <span>text</span> in a paragraph.</p>
</div>
            </div>
            <p>In this case, unlike LESS, CSS defaults all custom property values to nothing, causing them to be ignored when they are
            used. LESS requires declaration in the same or greater scope to use a variable and in this case would return an error as
            shown previously in this article.</p>
            <h3>Unintentional fallback to initial values</h3>
            <p>As <code>var()</code> is resolved at runtime it is considered by CSS parsers to be valid until the browser computes the
            actual value to be used, at which point it makes a final determination of validity. Normal properties with values that do
            not contain <code>var()</code> can be determined valid long before by simple syntax checking among other things. So what
            does this mean for us?</p>
            <p>It means non-custom properties which have been declared more than once may not work as expected if they are followed
            with a value that contains <code>var()</code>. This is best exemplified by the following:</p>
            <div class="example">
<pre class="sh_css">
span {
    color: red;
    color: var(--cerulean);
}
</pre>
<pre class="sh_html">
<span>some text we want to be colored</span>
</pre>
<div>
<span>some text we want to be colored</span>
</div>
            </div>
            <p>So what happened? It was supposed to be red, right? Well, no. CSS started with the declaration of <code>color</code>
            with the value red, and determined it was valid. It then moved on to the declaration with <code>var()</code> and determined
            it was also valid, replacing the previous declaration. When it came time to compute the actual value, the browser found out
            the custom property we referenced did not exist and declared the entire value invalid. However at this point as it had
            already discarded the previous value, it was unable to fall back to it.</p>
            <h2>Complex use-cases and abilities</h2>
            <p>While CSS's custom properties may not be able to perform math on the declared values without using <code>calc()</code>
            or act as arguments to mix-ins, you can use them in a variety of property values as part of existing syntax. A quick
            example would be to use it to define a repeating linear gradient, which I demonstrate below.</p>
            <div class="example">
<pre class="sh_css">
:root {
    --spacing: 5px;
}

span {
    background: repeating-linear-gradient(-45deg, #1dacd6, #1dacd6 var(--spacing), transparent var(--spacing), transparent calc(var(--spacing) * 2));
}
</pre>
<pre class="sh_html" style="margin-top: 1em">
<p>More <span>text</span> in a paragraph.</p>
</pre>
<div>
    <!-- Faked. Again. -->
    <p>More <span style="background: repeating-linear-gradient(-45deg, #1dacd6, #1dacd6 5px, transparent 5px, transparent calc(5px * 2))">text</span> in a paragraph.</p>
</div>
            </div>
            <p>In the above we could also replace the colors in the stops with <code>var()</code> and the browser would be quite fine
            with it. There is one caveat: there is no special case for the <code>content</code> property. Any value in a custom
            property must compute to valid syntax in order to show up. It does not auto-quotify them to display as text.</p>
            <h3>Default values</h3>
            <p>In a previous example in this article it was shown a browser will ignore the attempted use of a custom property which
            does not exist in the scope asking to use it. This is not always ideal. As such the <code>var()</code> function allows a
            default value to be specified which is used when the given custom property cannot be found. CSS considers any value after
            the first comma in a <code>var()</code> function call to be the default value. This means it can even include more commas.</p>
            <div class="example">
<pre class="sh_css">
span {
    color: var(--cerulean, lightblue);
}
</pre>
            </div>
            <h3>JavaScript access (CSSOM)</h3>
            <p>Finally, the second killer ability of CSS custom properties is their ability to be updated on the fly. Traditional
            pre-processors only perform transpiling once, baking in all variable values for the entire runtime of the page. The spec on
            the other hand gives us a way to update the value of these properties ourselves, and the change will be reflected on the
            page. This could prove to be quite handy. The following contrived example, using the gradient example's styles and markup,
            indicates how to do this.</p>
            <div class="example">
<pre class="sh_javascript_dom">
document.querySelector(":root").style.setProperty("--spacing", "3px");
</pre>
<div>
    <p>More <span style="background: repeating-linear-gradient(-45deg, #1dacd6, #1dacd6 3px, transparent 3px, transparent calc(3px * 2))">text</span> in a paragraph.</p>
</div>
            </div>
            <p>Next, the spec is quite <a href="https://www.w3.org/TR/css-variables-1/#syntax">permissive</a> in what it allows for the
            value of custom properties. Their stated intent for this inclusiveness is to enable potential future uses via JavaScript
            for those values, even if they are not further used within the stylesheet. An example of retrieving property values is
            given below. Be aware the returned value may include all text after the colon but before the semicolon, including any
            leading spaces. Values set by JavaScript won't have any leading spaces unless explicitly included in the value.</p>
            <div class="example">
<pre class="sh_css">
span {
    --highlights: 2 5, 4 10, 6 8, 20 35;
}
</pre>
<pre class="sh_javascript_dom" style="margin-top: 1em">
var highlights = window.getComputedStyle(document.querySelector("span")).getPropertyValue("--highlights");
</pre>
            </div>
            <p>I don't have a bunch of great examples on hand about how to go about using JS access usefully, but it might be
            interesting to try using it as a simple user theme generator. If some UI exposed all reasonable custom properties, they
            could be updated on the fly by JavaScript without having to write out a new custom stylesheet per-user. To see that and
            other things from this article in action, check the <a href="demo.html">demo page</a>.</p>
            <p>And that's it! Thank you for reading.</p>
            <h2>July Addendum: Another usage for JS updates</h2>
            <p>After originally writing this article, I eventually did find another interesting use-case for updating custom properties
            in JavaScript: CSS triangles used with message balloons. Message balloons are used in many places and frequently they have
            a triangle that points to the item they relate to. These triangles can be done with <code>:before</code> or
            <code>:after</code> pseudo-elements, at a fixed location. Such a balloon example is shown below:</p>
            <div class="example">
<style data-copy>
.balloon {
    position: relative;
    padding: 1em;
    background: hotpink;
    border-radius: 3px;
}

.balloon:after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 5px solid hotpink;
}
</style>
<div>
    <div class="balloon">Some text here.</div>
</div>
            </div>
            <p>This requires the balloon to move to keep the triangle next to or in the middle of the relevant element. Sometimes,
            however, it would be nicer if the triangle could move when the balloon cannot. This may happen in cases where the relevant
            element is close to the edge of the page or viewport and you do not wish to cut off the balloon. Unfortunately
            pseudo-elements are untouchable from JavaScript and we cannot change their styles on the fly. Custom properties are able to
            be updated by JavaScript and so can come to our rescue.</p>
            <div class="example">
<style data-copy>
.dynaballoon {
    --after: 50%;
    position: relative;
    padding: 1em;
    background: hotpink;
    border-radius: 3px;
}

.dynaballoon:after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    left: var(--after);
    margin-left: -5px;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 5px solid hotpink;
}
</style>
<style>
/* Faked because well, IE and all that. */
.dynaballoon:after {
    left: 15px;
}
</style>
<pre class="sh_javascript_dom">
document.querySelector(".dynaballoon")
        .style.setProperty("--after", "15px");
</pre>
<div>
    <div class="dynaballoon">Some text here.</div>
</div>
            </div>
            <p>This works great, but do remember the section from above about the processing order of <code>var</code>. In browsers
            that don't recognize this new feature, the value <code>50%</code> will be used. Those which do recognize it will throw out
            that "backup" value as <code>var</code> is considered to be valid at processing/parsing time. Therefore any invalid values
            passed by JavaScript will cause all properties where it is used to revert to their initial values, not <code>50%</code>.</p>
        </article>
        <nav>
            <a href="../../">Table of Contents</a> / <a href="#top">Return to top</a>
        </nav>
        <footer>
            Syntax highlighting provided by <a href="http://shjs.sourceforge.net/">SHJS</a>. Found a typo or bug?
            <a href="https://bitbucket.org/AMcBain/articles/issues?status=new&amp;status=open">Report it</a>. Please cite or attribute quotes and
            borrowed text; Code snippets are <a href="http://creativecommons.org/publicdomain/zero/1.0/">Public Domain / CC0</a>.
        </footer>
    </body>
</html>
