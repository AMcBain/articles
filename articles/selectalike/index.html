<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Articles - Selectalike: CSS-only select replacement</title>
        <link rel="stylesheet" href="../../resources/sh_bright.min.css">
        <script src="../../resources/sh_main.min.js"></script>
        <script src="../../resources/sh_javascript_dom.min.js"></script>
        <script src="../../resources/sh_css.min.js"></script>
        <script src="../../resources/sh_html.min.js"></script>
        <script src="../../resources/stringify.js"></script>
        <link rel="stylesheet" href="../../resources/styles.css">
    </head>
    <body>
        <nav>
            <a href="../../">Table of Contents</a>
        </nav>
        <article>
            <h1 id="top">Selectalike: A more styleable CSS-only select replacement</h1>
            <time pubdate>2013-08-12</time>
            <p class="notice">
                The concept in this article only works in IE9+ due to use of certain
                <a href="http://caniuse.com/#search=css3%20selectors">CSS3 selectors</a>. Other browsers have supported them for quite a while.
                It isn't impossible to make them IE8 compatible, but doing so is not covered by this article. If such support is needed and
                you aren't willing to adapt what is in this article, I would bail and look elsewhere.
            </p>
            <p>Form elements are notoriously hard to style. In theory, these elements are
            <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Replaced_element">replaced elements</a> and they shouldn't be
            styleable. Nevertheless browsers let us apply some properties such as borders, backgrounds, width, and other simple things on
            some such elements making it all the more annoying when we can't completely style them or different browsers inconsistently
            apply/lack those styles. There are various solutions to this, most of which involve JavaScript and cover the element up or ditch
            it entirely for more styleable elements. This article shows a CSS-only method to make a collection of radio buttons appear like a
            select and the advantages of doing so for accessibility and freedom of styles.</p>
            <h2>As it stands: the raw element</h2>
            <p>First let's take a look at a standard select as rendered by various browsers with custom styles applied. You can skip this
            section if you're already quite familiar with the failings of form element styling.</p>
            <div class="example">
<style data-copy>
select {
    background: #eee;
    border: 1px solid green;
    border-radius: 2px;
}

option:first-child + option {
    font-style: italic;
}
</style>
                <div>
                    <h3>Firefox 23 (Windows)</h3>
                    <img src="select-firefox.png" alt="select styling in Firefox on Windows">
                </div>
                <div>
                    <h3>Chrome 28 (Windows)</h3>
                    <img src="select-chrome.png" alt="select styling in Chrome on Windows">
                </div>
                <div>
                    <h3>IE10 (Windows 7)</h3>
                    <img src="select-ie10.png" alt="select styling in IE10 on Windows 7">
                </div>
                <div>
                    <h3>Your browser</h3>
                    <select>
                        <option>Item 1</option>
                        <option>Item 2</option>
                        <option>Item 3</option>
                        <option>Item 4</option>
                    </select>
                </div>
            </div>
            <p>It is quite easy to see that most styles are applied as asked for the main select body, but there remains some inconsistencies
            with the menu. Firefox was always the most liberal in applying select styles and lets us change the font-style. The rest ignore
            it. None of them respect the border styles for the menu, color or radius. Lastly, all have this unstyelable button we can't do
            anything about. (IE10 has one too, but it is covered up.)</p>
            <p>I think this is about as good as it gets for a raw select. Pretty ugly if you ask me. I'd much rather have the raw original if
            this is all we can do. This is not a dead end, however, more can be done. We just have to stop thinking about selects for a moment.</p>
            <h2>Finding analogues</h2>
            <p>The first step to getting better styles is to find another element compatible with forms so our user's selected information
            makes it back to the server without the aid of JavaScript. This limits us to other form elements, and reveals an obvious choice:</p>
            <ul class="example">
                <li>Radio buttons</li>
                <li><input id="radio11" type="radio" name="radio1"><label for="radio11">Item 1</label></li>
                <li><input id="radio12" type="radio" name="radio1"><label for="radio12">Item 2</label></li>
                <li><input id="radio13" type="radio" name="radio1"><label for="radio12">Item 3</label></li>
                <li><input id="radio14" type="radio" name="radio1"><label for="radio12">Item 4</label></li>
            </ul>
<pre class="sh_html">
<input id="item1" type="radio" name="items"><label for="item1">Item 1</label>
<input id="item2" type="radio" name="items"><label for="item2">Item 2</label>
<input id="item3" type="radio" name="items"><label for="item3">Item 3</label>
<input id="item4" type="radio" name="items"><label for="item4">Item 4</label>
</pre>
            <p>Radio buttons allow a user to select only one item out of many, even though they take up more space on the interface and are
            even harder to style. Lucky for us we're not after their styles but the behavior of labels when linked with an input via the for
            attribute. In all browsers, clicking on an associated label focuses the input it is linked with or selects it as appropriate for
            that input type. This is the first step to freedom: being able to place items which are very styleable anywhere we want while
            still storing the user's choice.</p>
            <h2>Building the replacement</h2>
            <p>Having found a suitable feature-equivalent set of elements, we can get back to thinking about selects.</p>
            <h3>Click-based toggles</h3>
            <p>The first and hardest part about replicating something like a select is the click-based behavior of it. Most replacements
            either trigger on hover, which doesn't seem quite natural, or use JavaScript to avoid the issue. What makes this task hard is the
            almost stateless nature of CSS and that existing states are just as easily repealed as they were applied (such as hover).</p>
            <p>Even so, there is one state which stands out as having the ability to stick around long enough to be useful and requires
            explicit user interaction to trigger: focus. It is better than hover at achieving our goal because an element retains it even if
            the user is no longer hovering the element and like the select, the focus state is removed when the user gives something else
            focus or clicks somewhere else.</p>
            <p>The following example demonstrates this behavior and the basics of direction towards the replacement we want. There's a lot in
            this example, so afterwards I will deconstruct it and briefly explain parts of the example and reasons for the choices made.</p>
            <div class="example" data-name="opentest">
<style id="opentest-css" data-copy>
.%selectalike {
    display: inline-block;
    height: 1.2em;
    vertical-align: bottom;
    border: 1px solid green;
}

.%selectalike input {
    visibility: hidden;
    position: absolute;
}

.%selectalike label {
    visibility: hidden;
    display: block;
}

.%selectalike:focus label {
    visibility: visible;
}
</style>
<div id="opentest-html" data-copy>
<h4>Live Example</h4>
<div class="%selectalike" tabindex="0">
    <input id="%value1" type="radio" name="%value">
    <input id="%value2" type="radio" name="%value">
    <input id="%value3" type="radio" name="%value">
    <input id="%value4" type="radio" name="%value">
    <label for="%value1">Item 1</label>
    <label for="%value2">Item 2</label>
    <label for="%value3">Item 3</label>
    <label for="%value4">Item 4</label>
</div>
<p>Click me, then somewhere else.</p>
</div>
            </div>
            <p>The properties applied to the div are to make it behave like an inline element with a fixed height to match a real select. If
            you open a select, the menu doesn't push down the items after it and we don't want to either. Due to the click forwarding of
            paired labels, we don't actually need the inputs themselves to be visible or even in the flow. They can be safely hidden where
            they can't interfere. We can't remove them entirely because, as mentioned earlier, we still need a form element to remain on the
            page to hold a value that gets sent to the server on form submission.</p>
            <p id="checktest">The remaining styles ensure the labels stack vertically, are hidden by default, and show up when their parent
            gets focus. We actually want the label that corresponds to the user's or default choice to stay visible when the parent doesn't
            have focus but that will be fixed later in this article. The reason for hiding each label instead of setting an overflow on the
            parent is to give us more flexibility as to what happens to the labels after focus is lost on the parent. Overflow would cause
            the children to disappear immediately preventing processing of some events. For example, Firefox will process focus changes from
            a click and all effects of the focus change before it notifies elements a click occurred. If the focus change caused the element
            to move out from under the cursor or disappear, Firefox will not dispatch a click event to it. In the case of our labels this
            would prevent the associated inputs from being selected. This will be important to remember when we get around to closing the
            menu on click.</p>
            <div class="example">
<style data-copy>
.checktest {
    height: 2.4em;
    overflow: hidden;
    border: 1px solid green;
}

.checktest:focus {
    overflow: visible;
}
</style>
<div data-copy>
<h4>Live Example</h4>
<div class="checktest" tabindex="0">
    <p>Click me first, then the checkbox.</p>
    <input type="checkbox" id="checkbox">
    <label for="checkbox">Checkbox</label>
</div>
</div>
                <aside>If you click the label and not the checkbox itself, it may actually work. However it doesn't work in the final
                replacement, possibly due to the input being hidden, and it is better to work around it to ensure it always works in any
                case.</aside>
            </div>
            <p>But wait, what about that vertical-align? I honestly don't know. Without it, the children push any items after the div down
            even though I don't think they should due to the height set on the parent. It's magic. If anyone knows, please let me know and I
            will edit this article with what it does.</p>
            <p>On the HTML side of things, the <code>tabindex</code> attribute on the div is quite important. It tells
            the browser the div is allowed to gain focus, even though it normally can't. The
            <a href="http://www.w3.org/TR/html401/interact/forms.html#h-17.11.1">value of zero</a> indicates the div is to be included in the
            focus navigation cycle based on where it sits in the flow: after the last focusable element, but before the next. Lastly, it
            shouldn't have escaped notice that I also reordered the children. The inputs have been moved before and away from their labels.
            This is because we want to be able to select certain labels based on the checked state of any input, which cannot be done if any
            of the inputs are ordered past at least the first label in the source. This will make more sense in the next section.</p>
            <h3>Showing user selection</h3>
            <p>The basic code above gets us on our way, but doesn't show the value the user clicked on. Borrowing those styles and the same
            markup (excepting for the unique ID requirement) we can add some selectors to accomplish this. Since all the labels are in the
            flow we can easily move the selected label in to view by changing the top margin of the first label on the assumption that all
            labels are the same height:</p>
            <div class="example" data-name="usertest">
                <style data-href="opentest-css"></style>
<style id="usertest-css" data-copy>
.%selectalike label {
    height: 1.2em;
    line-height: 1.2em;
}

.%selectalike input:nth-child(2):checked ~ label:first-of-type {
    margin-top: -1.2em;
}

.%selectalike input:nth-child(3):checked ~ label:first-of-type {
    margin-top: -2.4em;
}

.%selectalike input:nth-child(4):checked ~ label:first-of-type {
    margin-top: -3.6em;
}

.%selectalike input:nth-child(1):checked ~ label:nth-of-type(1),
.%selectalike input:nth-child(2):checked ~ label:nth-of-type(2),
.%selectalike input:nth-child(3):checked ~ label:nth-of-type(3),
.%selectalike input:nth-child(4):checked ~ label:nth-of-type(4) 
{
    visibility: visible;
}
</style>
                <div class="sh_html" style="margin-top: 1.25em" data-href="opentest-html"></div>
            </div>
            <p>This works pretty well except for the use of some long selectors, the number of which is dependent on the number of labels.
            The reason for this is because the offset by which the children are moved needs to increase with every label to bring it inside
            the parent's visible area. The second set of selectors has a similar issue: only the label corresponding to the selected input
            should stay visible when focus is removed from the parent. Since I cannot select a label by its for attribute based on the value
            of an input's id attribute, I have to write a selector for each pair explicitly. Unfortunate. I don't believe all of this long
            set of selectors could be avoided if this replacement were top-anchored&mdash;where the menu doesn't shift based on the chosen
            value like the real select&mdash;as it seems the last selector block would still be needed.</p>
            <p>Secondly, to deal with the problem of needing <span>n</span> selectors for <span>n</span> labels a large number could be
            pre-defined up to cover any number of potential lengths that might be encountered on a site. Not the greatest solution. They
            could be generated via script instead, but that defeats the idea of being CSS-only. If only I could could use the CSS placeholder
            value "n" and have it mean the same value in both parts of the selector. Then I would only need one line for the second selector
            block. Alas CSS only offers to treat them independently.</p>
            <p>Finally we should deal with the issue of the rectangle appearing blank by default. This is easily solved by the making one of
            the inputs selected by default in the markup with the addition of a checked attribute.</p>
            <div class="example" data-name="defaultest">
                <style data-href="opentest-css"></style>
                <style data-href="usertest-css"></style>
<div data-copy>
<h4>Live Example</h4>
<div class="%selectalike" tabindex="0">
    <input id="%dvalue1" type="radio" name="%value" checked="checked">
    <input id="%dvalue2" type="radio" name="%value">
    <input id="%dvalue3" type="radio" name="%value">
    <input id="%dvalue4" type="radio" name="%value">
    <label for="%dvalue1">Item 1</label>
    <label for="%dvalue2">Item 2</label>
    <label for="%dvalue3">Item 3</label>
    <label for="%dvalue4">Item 4</label>
</div>
<p>Click me, then somewhere else.</p>
</div>
            </div>
            <h3>Auto-closing</h3>
            <p>By now it's probably obvious the menu doesn't close when a value is clicked on, only when focus on the parent is lost. Real
            selects don't behave this way so we should modify the behavior of our replacement. The first step to doing this is give the
            labels inside the ability to steal focus. Ordinarily the inputs associated with the labels would steal focus when the label is
            clicked, but this doesn't happen when the inputs are hidden. The same trick that makes the parent focusable can be used here:</p>
            <div class="example" data-name="closetest">
<pre id="closetest-html" class="sh_html">
<h4>Live Example</h4>
<div class="%selectalike" tabindex="0">
    <input id="%cvalue1" type="radio" name="%value" checked="checked">
    <input id="%cvalue2" type="radio" name="%value">
    <input id="%cvalue3" type="radio" name="%value">
    <input id="%cvalue4" type="radio" name="%value">
    <label for="%cvalue1" tabindex="0">Item 1</label>
    <label for="%cvalue2" tabindex="0">Item 2</label>
    <label for="%cvalue3" tabindex="0">Item 3</label>
    <label for="%cvalue4" tabindex="0">Item 4</label>
</div>
</pre>
                <p>Once the labels are focusable, we need to add a few selectors to compensate for the disappear-before-click problem noted
                in a <a href="#checktest">prior section</a>. One way to do this is, as below, make a selector which keeps the label visible
                while the cursor is over it so it stays long enough for the click to fully register.</p>
                <style data-href="opentest-css"></style>
                <style data-href="usertest-css"></style>
<style id="closetest-css1" data-copy>
.%selectalike label {
    position: relative;
}

.%selectalike label:hover {
    visibility: visible;
    z-index: auto;
}
</style>
                <p>As soon as the the input becomes selected, though, the label should hide itself behind the parent again. If it doesn't,
                it will become impossible to select the parent again to open the menu as it will be covered by the label. This is easy enough
                to do by modifying a selector used previously:</p>
<style id="closetest-css2" data-copy>
.%selectalike input:nth-child(1):checked ~ label:nth-of-type(1),
.%selectalike input:nth-child(2):checked ~ label:nth-of-type(2),
.%selectalike input:nth-child(3):checked ~ label:nth-of-type(3),
.%selectalike input:nth-child(4):checked ~ label:nth-of-type(4) 
{
    visibility: visible;
    z-index: -1;
}
</style>
                <aside>As an aside, if you haven't seen a z-index of -1 before, it seems to be rather special in practice. I don't recall
                any other negative values working and browsers order items with -1 before their own parent. This only works if the parent
                doesn't have its own z-index.</aside>
                <p>Unfortunately now that the previous selector is quite specific, the labels associated with checked labels won't come to
                the top when the parent is focused. This is desirable because the currently selected value can be selected again to close the
                menu without having to click somewhere else or select a different value. The following monstrous selector is useful to this
                end. It is only so long to be more specific without using !important which would allow a shorter selector.</p>
<style id="closetest-css3" data-copy>
.%selectalike[tabindex]:focus > input:nth-child(n) ~ label:nth-of-type(n) {
  z-index: auto;
}
</style>
                <p>Finally all of those can be put together to realize a quite useful replacement.</p>
                <div data-href="closetest-html" style="min-height: 8em"></div>
                <aside>Beware <span>overflow: hidden</span>. It is not your friend with this replacement. As it is only pure HTML and CSS it
                is subject to the rules of overflow. If the parent is not large enough to contain all the menu items in either direction, it
                will be clipped. Because of this, and that it doesn't handle overflow on itself for large numbers of values, I do not
                recommend using this for listing all 50 US states and territories or the
                <a href="http://geography.about.com/cs/countries/a/numbercountries.htm">~196</a> countries in the world. Until someone
                invents a pure CSS select replacement that is top-anchored, these are your largest limitations. Not bad.</aside>
            </div>
            <h2>Styling and polish</h2>
            <p>&hellip; except it's ugly. Don't tell me you didn't think that. It looks terrible. Nobody would use it as-is on a site, but that's
            fixable! This section contains suggestions for styling to make the replacement look great. However you're free to do anything you
            want for styles as long as the core parts most of this article is dedicated to are still present.</p>
            <h3>Basic menu styles</h3>
            <p>These styles can be integrated with previous selectors; I didn't copy in their content for brevity. As a quick overview, these
            styles give the replacement a darker border on focus and around the labels as well as a background for all the labels.</p>
            <div class="example" data-name="menupolish">
                <style data-href="opentest-css"></style>
                <style data-href="usertest-css"></style>
                <style data-href="closetest-css1"></style>
                <style data-href="closetest-css2"></style>
                <style data-href="closetest-css3"></style>
<style id="menupolish-css1" data-copy>
.%selectalike {
  font-size: .9em;
  border: 1px solid lightgray;
  border-radius: 2px;
}

.%selectalike label {
    top: -1px;
    z-index: -1;
    margin-left: -1px;
    margin-right: -1px;
    padding-left: .35em;
    padding-right: .35em;
    white-space: nowrap;
    background: white;
    border: 1px solid transparent;
    border-top-width: 0;
    border-bottom-width: 0;
}
</style>
<style id="menupolish-css2" data-copy>
.%selectalike input + label {
    border-top-width: 1px;
}

.%selectalike:focus, .%selectalike:focus label {
    border-color: darkgray;
}

.%selectalike:focus input + label {
    border-top-width: 1px;
    border-radius: 2px 2px 0 0;
}

.%selectalike:focus label:last-child {
    border-bottom-width: 1px;
    border-radius: 0 0 2px 2px;
}
</style>
                <style>
                    #h4adjust h4 {
                        margin-bottom: 3em;
                    }
                </style>
                <div id="h4adjust" data-href="closetest-html"></div>
            </div>
            <h3>Affordances and other final touches</h3>
            <p>It's a nice rectangle, but I think a bit better can be done. It doesn't quite look like a select. As a user, I might not know
            that I can click on it or what it does. The general concept of how an object makes it obvious what to do with the object is known
            as an <a href="http://en.wikipedia.org/wiki/Affordance">affordance</a>. One way to provide this is a triangle, like the original
            select, to indicate that when clicked a menu will appear. Since it moves in both directions I prefer to have two small arrows but
            this is up to personal preference.</p>
            <div class="example" data-name="finaltest">
                <style data-href="opentest-css"></style>
                <style data-href="usertest-css"></style>
                <style data-href="closetest-css1"></style>
                <style data-href="closetest-css2"></style>
                <style data-href="closetest-css3"></style>
                <style data-href="menupolish-css1"></style>
                <style data-href="menupolish-css2"></style>
<style data-copy>
.%selectalike label {
    padding-right: 1.35em;
}

.%selectalike label:before, .%selectalike label:after {
    visibility: hidden;
    position: absolute;
    right: 0;
}

.%selectalike label:before {
    content: "\25b4";
    top: -.2em;
}

.%selectalike label:after {
    content: "\25be";
    bottom: -.2em;
}
</style>
                <p>The previous styles hide the :before and :after elements because they should only show up on the label that sits inside
                the visible box of the parent when closed. The following selector does that, but is also in addition to the previous
                selectors using nth-child and nth-of-type and not a replacement even though they look similar.</p>
<style data-copy>
.%selectalike input:nth-child(1):checked ~ label:nth-of-type(1):before, .%selectalike input:nth-child(1):checked ~ label:nth-of-type(1):after,
.%selectalike input:nth-child(2):checked ~ label:nth-of-type(2):before, .%selectalike input:nth-child(2):checked ~ label:nth-of-type(2):after,
.%selectalike input:nth-child(3):checked ~ label:nth-of-type(3):before, .%selectalike input:nth-child(3):checked ~ label:nth-of-type(3):after,
.%selectalike input:nth-child(4):checked ~ label:nth-of-type(4):before, .%selectalike input:nth-child(4):checked ~ label:nth-of-type(4):after
{
    visibility: visible;
}
</style>
                <p>This selector could be avoided if the before and after were placed on the parent instead, but since selectors cannot walk
                back up the tree there is no way to color it white when hovering the label inside the parent's box which is nicer when adding
                a hover effect to labels as shown below.</p>
<style data-copy>
.%selectalike label:hover:before, .%selectalike label:hover:after {
    color: HighlightText;
}

.%selectalike:focus label:hover {
    color: HighlightText;
    background: highlight;
}
</style>
                <aside>CSS3 has deprecated the named system colors, for what reasons I don't know, so in theory it is advised to change the
                hover color used here. However I doubt that support for them will be removed for a while seeing as all the browsers I know of
                already support them. If you are doing your own styling with a different color scheme, then that would be a great time to
                change the hover color.</aside>
                <div data-href="closetest-html" style="clear: left; min-height: 8em; padding-top: 1em"></div>
            </div>
            <p>and there you have it! A complete select replacement that not only looks good, but you can style it to your heart's
            content. For a complete all-in-one example of this, please see the <a href="demo.html">demo page</a>. Thank you for reading.</p>
            <p class="notice">
                <strong>Credits:</strong> <a href="http://reisio.com/">reisio</a> from #css was originally working on the concept as a
                hover-triggered replacement before I decided to take it more closely to the select using focus.
            </p>
        </article>
        <nav>
            <a href="../../">Table of Contents</a> / <a href="#top">Return to top</a>
        </nav>
        <footer>
            Syntax highlighting provided by <a href="http://shjs.sourceforge.net/">SHJS</a>. Found a typo or bug?
            <a href="https://bitbucket.org/AMcBain/articles/issues?status=new&amp;status=open">Report it</a>. Please cite or attribute quotes and
            borrowed text; Code snippets are <a href="http://creativecommons.org/publicdomain/zero/1.0/">Public Domain / CC0</a>.
        </footer>
    </body>
</html>
