// This was uglified, but I figure if people wanted to know what it was, they would anyway. So I left the shame in full view.
window.onload = function ()
{
    var i, s, e, magic = /%[a-z]+/g, stupid = /checked="checked" type="([^"]+)"/g, stupidRepl = 'type="$1" checked="checked"';

    function set(e, t)
    {
        if ("textContent" in e)
        {
            e.textContent = t;
        }
        else
        {
            e.innerText = t;
        }
    }

    e = document.querySelectorAll("pre.sh_html")
    for (i = 0; i < e.length; i++)
    {
        e[i].className += " later";
    }

    e = document.querySelectorAll("style[data-copy]")
    for (i = 0; i < e.length; i++)
    {
        s = document.createElement("pre");
        s.className = "sh_css";
        set(s, e[i].innerHTML.replace(/([^\d])%/g, "$1").replace(/\r?\n/, ""));
        e[i].parentNode.insertBefore(s, e[i].nextSibling);
    }

    e = document.querySelectorAll("div[data-copy]");
    for (i = 0; i < e.length; i++)
    {
        s = document.createElement("pre");
        s.className = "sh_html";
        set(s, e[i].innerHTML.replace(/([^\d])%/g, "$1").replace(/\r?\n/, "").replace(stupid, stupidRepl));
        e[i].parentNode.insertBefore(s, e[i]);
    }

    e = document.querySelectorAll("style[data-href]");
    for (i = 0; i < e.length; i++)
    {
        s = document.getElementById(e[i].getAttribute("data-href"));
        e[i].appendChild(document.createTextNode((s.innerText || s.textContent).replace(magic, e[i].parentNode.getAttribute("data-name"))));
    }

    e = document.querySelectorAll("div[data-href]");
    for (i = 0; i < e.length; i++)
    {
        s = document.getElementById(e[i].getAttribute("data-href"));
        e[i].innerHTML = s.innerHTML.replace(magic, e[i].parentNode.getAttribute("data-name"));
    }

    e = document.querySelectorAll("style[data-copy]")
    for (i = 0; i < e.length; i++)
    {
        e[i].innerHTML = e[i].innerHTML.replace(magic, e[i].parentNode.getAttribute("data-name"));
    }

    e = document.querySelectorAll("div[data-copy]")
    for (i = 0; i < e.length; i++)
    {
        e[i].innerHTML = e[i].innerHTML.replace(magic, e[i].parentNode.getAttribute("data-name"));
    }

    e = document.querySelectorAll(".later")
    for (i = 0; i < e.length; i++)
    {
        set(e[i], e[i].innerHTML.replace(/([^\d])%/g, "$1").replace(stupid, stupidRepl));
    }

    // Now run the highlighter after all the magic is done.
    sh_highlightDocument();
};

// I don't like IE8. I can't deny it might exist out there. It is possible some of my articles might still work in IE8 or
// someone might visit them from IE8. They should really upgrade, though ...
document.createElement("nav");
document.createElement("time");
document.createElement("article");
document.createElement("aside");
